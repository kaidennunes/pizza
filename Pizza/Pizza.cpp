// Pizza.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

const double PI = 3.14159;

const int LARGE_PIZZA_DIAMETER = 20;
const int MEDIUM_PIZZA_DIAMETER = 16;
const int SMALL_PIZZA_DIAMETER = 12;

const int NUMBER_OF_GUESTS_PER_LARGE_PIZZA = 7;
const int NUMBER_OF_GUESTS_PER_MEDIUM_PIZZA = 3;
const int NUMBER_OF_GUESTS_PER_SMALL_PIZZA = 1;

const double LARGE_PIZZA_COST = 14.68;
const double MEDIUM_PIZZA_COST = 11.48;
const double SMALL_PIZZA_COST = 7.28;

/*
	How many guests are attending?
	2
	What percentage of a tip do you want to give?
	12
	People				2
	Tip					12%
	Larges				0
	Mediums				0
	Smalls				2
	Area (in2)			226.194
	Area/Person (in2)	113.097
	Cost ($)			$16


	How many guests are attending?
	150
	What percentage of a tip do you want to give?
	16
	People				150
	Tip					16%
	Larges				21
	Mediums				1
	Smalls				0
	Area (in2)			6798.4
	Area/Person (in2)	45.3227
	Cost ($)			$371


	How many guests are attending?
	57
	What percentage of a tip do you want to give?
	21
	People				57
	Tip					21%
	Larges				8
	Mediums				0
	Smalls				1
	Area (in2)			2626.37
	Area/Person (in2)	46.0767
	Cost ($)			$151




*/
int main()
{
	cout << "How many guests are attending?" << endl;
	int numberOfGuests;
	cin >> numberOfGuests;
	const int NUMB_OF_GUESTS = numberOfGuests;
	int remainingNumbOfGuests = NUMB_OF_GUESTS;

	// Find number of every type of pizza
	const int NUMB_OF_LARGE_PIZZAS = remainingNumbOfGuests / NUMBER_OF_GUESTS_PER_LARGE_PIZZA;
	remainingNumbOfGuests -= NUMB_OF_LARGE_PIZZAS * NUMBER_OF_GUESTS_PER_LARGE_PIZZA;
	const int NUMB_OF_MEDIUM_PIZZAS = remainingNumbOfGuests / NUMBER_OF_GUESTS_PER_MEDIUM_PIZZA;
	remainingNumbOfGuests -= NUMB_OF_MEDIUM_PIZZAS * NUMBER_OF_GUESTS_PER_MEDIUM_PIZZA;
	// remainingNumbOfGuests will equal two, one, or zero
	// For these values we have to get small pizzas, which is one per remaining guest
	const int NUMB_OF_SMALL_PIZZAS = remainingNumbOfGuests;

	// Calculate the area of the pizzas
	// ((pow(LARGE_PIZZA_DIAMETER, 2)) / 4)) is the diamater equivalent of radius squared
	const double TOTAL_AREA_OF_LARGE_PIZZAS = (PI * ((pow(LARGE_PIZZA_DIAMETER, 2)) / 4)) * NUMB_OF_LARGE_PIZZAS;
	const double TOTAL_AREA_OF_MEDIUM_PIZZAS = PI * ((pow(MEDIUM_PIZZA_DIAMETER, 2)) / 4) * NUMB_OF_MEDIUM_PIZZAS;
	const double TOTAL_AREA_OF_SMALL_PIZZAS = PI * ((pow(SMALL_PIZZA_DIAMETER, 2)) / 4) * NUMB_OF_SMALL_PIZZAS;
	
	// Add areas of pizzas together to find the total area
	const double TOTAL_AREA_OF_PIZZAS = TOTAL_AREA_OF_SMALL_PIZZAS + TOTAL_AREA_OF_MEDIUM_PIZZAS + TOTAL_AREA_OF_LARGE_PIZZAS;

	// Find area of pizza each person can eat
	const double AREA_OF_PIZZA_PER_PERSON = TOTAL_AREA_OF_PIZZAS / NUMB_OF_GUESTS;

	// Find how big a tip they want to give
	cout << "\nWhat percentage of a tip do you want to give?" << endl;
	int tippingPercentage;
	cin >> tippingPercentage;
	const double TIP_PERCENTAGE= tippingPercentage;

	// Calculate the cost of the pizzas
	const double TOTAL_COST_OF_LARGE_PIZZAS = NUMB_OF_LARGE_PIZZAS * LARGE_PIZZA_COST;
	const double TOTAL_COST_OF_MEDIUM_PIZZAS = NUMB_OF_MEDIUM_PIZZAS * MEDIUM_PIZZA_COST;
	const double TOTAL_COST_OF_SMALL_PIZZAS = NUMB_OF_SMALL_PIZZAS * SMALL_PIZZA_COST;

	// Add the cost of the pizzas together
	const double TOTAL_COST_OF_PIZZAS = TOTAL_COST_OF_SMALL_PIZZAS + TOTAL_COST_OF_MEDIUM_PIZZAS + TOTAL_COST_OF_LARGE_PIZZAS;

	// Add the cost of the tip
	const double TOTAL_COST_OF_MEAL = round((TOTAL_COST_OF_PIZZAS) * ((TIP_PERCENTAGE / 100.0) + 1));


	cout << "\nPeople\t\t\t";
	cout << NUMB_OF_GUESTS << endl;

	cout << "Tip\t\t\t";
	cout << TIP_PERCENTAGE << "%" << endl;

	cout << "Larges\t\t\t";
	cout << NUMB_OF_LARGE_PIZZAS << endl;

	cout << "Mediums\t\t\t";
	cout << NUMB_OF_MEDIUM_PIZZAS << endl;

	cout << "Smalls\t\t\t";
	cout << NUMB_OF_SMALL_PIZZAS << endl;

	cout << "Area (in2)\t\t";
	cout << TOTAL_AREA_OF_PIZZAS << endl;

	cout << "Area/Person (in2)\t";
	cout << AREA_OF_PIZZA_PER_PERSON << endl;

	cout << "Cost ($)\t\t";
	cout << "$" << TOTAL_COST_OF_MEAL << endl;

	system("pause");
	return 0;
}


